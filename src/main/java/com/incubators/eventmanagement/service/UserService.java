package com.incubators.eventmanagement.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.incubators.eventmanagement.data.model.SubEvent;
import com.incubators.eventmanagement.data.model.SubeventHistory;
import com.incubators.eventmanagement.data.model.User;
import com.incubators.eventmanagement.data.repository.EventRepository;
import com.incubators.eventmanagement.data.repository.SubEventRepository;
import com.incubators.eventmanagement.data.repository.SubeventHistoryRepository;
import com.incubators.eventmanagement.data.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubeventHistoryRepository subeventHistoryRepository;

    @Autowired
    private SubEventRepository subEventRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private AdminService adminService;

    public ResponseEntity<?> signUp(Map<String, String> entity) {

        if (userRepository.existsById(entity.get("MobileNumber"))) {

            Map<String, String> response = new HashMap<>();
            response.put("key", "Mobile Number already in use ");
            return ResponseEntity.ok().body(response);

        }
        User user = new User();
        user.setMobileNumber(entity.get("MobileNumber"));
        user.setFirstName(entity.get("FirstName"));
        user.setLastName(entity.get("LastName"));
        user.setPassword(entity.get("Password"));
        user.setSecretQuestion(entity.get("SecretQuestion"));
        user.setAnswer(entity.get("Answer"));
        user.setLogInStatus(0);
        userRepository.save(user);
        Map<String, String> response = new HashMap<>();
        response.put("key", "User Added");
        return ResponseEntity.ok().body(response);

    }

    public ResponseEntity<?> logIn(Map<String, String> entity) {
        // Check User is admin
        if(adminService.isAdmin(entity)){
            return adminService.adminLogin(entity);
        }

        // Check user is registered
        if (!userRepository.existsById(entity.get("MobileNumber"))) {

            Map<String, String> response = new HashMap<>();
            response.put("key", "Sign up first");
            return ResponseEntity.ok().body(response);

        }

        User user = userRepository.findById(entity.get("MobileNumber")).get();

        // check user already logged in
        if (user.getLogInStatus() == 1) {
            Map<String, String> response = new HashMap<>();
            response.put("key", "already logged in");
            return ResponseEntity.ok().body(response);
        }

        // check user password
        if (!user.getPassword().equals(entity.get("Password"))) {

            Map<String, String> response = new HashMap<>();
            response.put("key", "Invalid password");
            return ResponseEntity.ok().body(response);

        }

        user.setLogInStatus(1);
        userRepository.save(user);

        Map<String, String> response = new HashMap<>();
        response.put("key", "successful");

        return ResponseEntity.ok().body(response);

    }

    public ResponseEntity<?> forgotPassword(Map<String, String> entity) {
        // Check user is registered
        if (!userRepository.existsById(entity.get("MobileNumber"))) {

            Map<String, String> response = new HashMap<>();
            response.put("Error", "Sign up first");
            return ResponseEntity.ok().body(response);

        }

        User user = userRepository.findById(entity.get("MobileNumber")).get();

        if (!user.getSecretQuestion().equals(entity.get("SecretQuestion"))) {
            Map<String, String> response = new HashMap<>();
            response.put("Error", "Secret Question doesn't match");
            return ResponseEntity.ok().body(response);
        }

        if (!user.getAnswer().equals(entity.get("Answer"))) {
            Map<String, String> response = new HashMap<>();
            response.put("Error", "Answer doesn't match");
            return ResponseEntity.ok().body(response);
        }

        user.setPassword(entity.get("NewPassword"));
        userRepository.save(user);

        Map<String, String> response = new HashMap<>();
        response.put("Message", "successful");

        return ResponseEntity.ok().body(response);
    }

    public ResponseEntity<?> bookTicket(Map<String, String> entity) {

        // LocalTime lt = LocalTime.of(00, 12);
        // System.out.println(LocalTime.now().isAfter(lt));

        // LocalDate ld = LocalDate.of(2023, 3, 26);
        // System.out.println(LocalDate.now().isAfter(ld));

        // Check user is registered
        if (!userRepository.existsById(entity.get("MobileNumber"))) {

            Map<String, String> response = new HashMap<>();
            response.put("key", "Sign up first");
            return ResponseEntity.ok().body(response);

        }

        String MobileNumber = entity.get("MobileNumber");
        String SubeventName = entity.get("SubeventName");
        String MainEventName = entity.get("MaineventName");

        if (!subeventHistoryRepository.existsByMobileNumberAndSubeventNameAndMainEventName(MobileNumber, SubeventName, MainEventName).isEmpty()) {
            Map<String, String> response = new HashMap<>();
            response.put("key", "Already Booked");
            return ResponseEntity.ok().body(response);
        }

        SubeventHistory subeventHistory = new SubeventHistory();
        subeventHistory.setMainEventName(MainEventName);
        subeventHistory.setSubeventName(SubeventName);
        subeventHistory.setMobileNumber(MobileNumber);
        subeventHistoryRepository.save(subeventHistory);

        SubEvent subEvent =  subEventRepository.findByEventIdAndSubEventName(eventRepository.findByeventName(MainEventName).getId(), SubeventName);
        subEvent.setSoldTickets(subEvent.getSoldOut()+1);
        subEventRepository.save(subEvent);
        Map<String, String> response = new HashMap<>();
        response.put("key", "successful");

        return ResponseEntity.ok().body(response);
    }

    public ResponseEntity<?> addRating(Map<String, String> entity) {

        String MobileNumber = entity.get("MobileNumber");
        String SubeventName = entity.get("SubeventName");
        String MainEventName = entity.get("MaineventName");
        //String Rating = entity.get("Rating");
        //String Feedback = entity.get("Feedback");

        if (subeventHistoryRepository
                .existsByMobileNumberAndSubeventNameAndMainEventName(MobileNumber, SubeventName, MainEventName)
                .isEmpty()) {

            Map<String, String> response = new HashMap<>();
            response.put("key", "Event not booked yet");
            return ResponseEntity.ok().body(response);
        }

        List<SubeventHistory> list = subeventHistoryRepository.existsByMobileNumberAndSubeventNameAndMainEventName(MobileNumber, SubeventName, MainEventName);
        list.get(0).setRating(entity.get("Rating"));
        list.get(0).setFeedback((entity.get("Feedback")));
        subeventHistoryRepository.save(list.get(0));

        Map<String, String> response = new HashMap<>();
        response.put("key", "successful");

        return ResponseEntity.ok().body(response);
    }

}

// int timeHour = Integer.parseInt(entity.get("timeHour")) ;
// int timeMinutes = Integer.parseInt(entity.get("timeMinutes")) ;

// int date = Integer.parseInt(entity.get("date"));
// int month = Integer.parseInt(entity.get("month"));
// int year =Integer.parseInt(entity.get("year")) ;

// LocalTime.now().getHour()
// LocalTime.now().getMinute()

// LocalDate.now().getDayOfMonth()
// LocalDate.now().getMonthValue()
// LocalDate.now().getYear()