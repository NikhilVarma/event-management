package com.incubators.eventmanagement.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.TypedSort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.incubators.eventmanagement.data.model.Event;
import com.incubators.eventmanagement.data.model.SubEvent;
import com.incubators.eventmanagement.data.repository.EventRepository;
import com.incubators.eventmanagement.data.repository.SubEventRepository;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SubEventRepository subEventRepository;

    public ResponseEntity<?> getLatestEvents(Map<String, String> entity) {

        TypedSort<Event> event = Sort.sort(Event.class);
        Sort sort = event.by(Event::getStartYear).ascending().and(event.by(Event::getStartMonth).ascending().and(event.by(Event::getStartDay).ascending()));
        Pageable pageable = PageRequest.of(Integer.parseInt(entity.get("pageNum")), 3, sort);

        return ResponseEntity.ok().body(eventRepository.findAll(pageable).getContent());

    }

    public ResponseEntity<?> getSearchEvent(Map<String, String> entity) {
        if (!eventRepository.existsByeventName(entity.get("eventname"))) {

            Map<String, String> response = new HashMap<>();
            response.put("Message", "Event Not Found");

            return ResponseEntity.ok().body(response);
        }

        return ResponseEntity.ok().body(eventRepository.findByeventName(entity.get("eventname")) );
    }


    public ResponseEntity<?> getFeaturedEvents(Map<String, String> entity) {

        TypedSort<Event> event = Sort.sort(Event.class);
        Sort sort = event.by(Event::getStartYear).ascending()
                .and(event.by(Event::getStartMonth).ascending().and(event.by(Event::getStartDay).ascending()));
        Pageable pageable = PageRequest.of(Integer.parseInt(entity.get("pageNum")), 3, sort);

        return  ResponseEntity.ok().body(eventRepository.findFeaturedEvent(pageable).getContent());
    }

    public ResponseEntity<?> getLatestSubEvents(Map<String, String> entity) {

        TypedSort<SubEvent> Subevent = Sort.sort(SubEvent.class);
       Sort sort = Subevent.by(SubEvent::getStartYear).ascending().and(Subevent.by(SubEvent::getStartMonth).ascending().and(Subevent.by(SubEvent::getStartDay).ascending().and(Subevent.by(SubEvent::getStartHour).ascending().and(Subevent.by(SubEvent::getStartMinute).ascending()))));
        Pageable pageable = PageRequest.of(Integer.parseInt(entity.get("pageNum")), 3, sort);

        return ResponseEntity.ok().body(subEventRepository.findAll(pageable).getContent());

    }

    public ResponseEntity<?> getSearchSubEvents(Map<String, String> entity) {

        if(!eventRepository.existsByeventName(entity.get("eventname"))){
            Map<String, String> response = new HashMap<>();
            response.put("Message", "Main Event Not Found");

            return ResponseEntity.ok().body(response);
        }
        int id  = eventRepository.findByeventName(entity.get("eventname")).getId();

        if (!subEventRepository.existsByEventIdAndSubEventName(id , entity.get("subeventname"))) {

            Map<String, String> response = new HashMap<>();
            response.put("Message", "Sub Event Not Found");

            return ResponseEntity.ok().body(response);
        }
 
        return ResponseEntity.ok().body(subEventRepository.findByEventIdAndSubEventName(id,entity.get("subeventname")) );
    }
}
