package com.incubators.eventmanagement.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.incubators.eventmanagement.data.model.Admin;
import com.incubators.eventmanagement.data.model.Event;
import com.incubators.eventmanagement.data.model.SubEvent;
import com.incubators.eventmanagement.data.repository.EventRepository;
import com.incubators.eventmanagement.data.repository.SubEventRepository;
import com.incubators.eventmanagement.data.repository.SubeventHistoryRepository;

@Service
public class AdminService {
    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SubEventRepository subEventRepository;

    @Autowired
    private SubeventHistoryRepository subeventHistoryRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private Admin admin;

    //Admin Login
    public boolean isAdmin(Map<String,String> ent){
        if(ent.get("MobileNumber").equals(admin.getAdmin())){
            return true;
        }
        return false;
    }

    public ResponseEntity<?> adminLogin(Map<String,String> ent){
        if(ent.get("Password").equals(admin.getPass())){

            Map<String, String> response = new HashMap<>();
            response.put("key", "Admin login successful");
            return ResponseEntity.ok().body(response);
        }
        Map<String, String> response = new HashMap<>();
        response.put("key", "Invalid Admin password");
        return ResponseEntity.ok().body(response);
    }

    // Create Event
    public ResponseEntity<?> createEvent(Map<String, String> ent) {
        if (eventRepository.existsByeventName(ent.get("eventname"))) {
            Map<String, String> response = new HashMap<>();
            response.put("key", "Event Already Exist");
            return ResponseEntity.ok().body(response);
        }

        Event event = new Event();
        event.setEventName(ent.get("eventname"));
        event.setStartDate(ent.get("startdate"));
        event.setEndDate(ent.get("enddate"));
        event.setLocation(ent.get("location"));
        event.setOrganizerName(ent.get("organizername"));
        eventRepository.save(event);

        Map<String, String> response = new HashMap<>();
        response.put("key", "Event created successfully");
        return ResponseEntity.ok().body(response);
    }

    // Add Sub Event
    public ResponseEntity<?> addSubEvent(Map<String, String> ent) {

        if (!eventRepository.existsByeventName(ent.get("eventname"))) {

            Map<String, String> response = new HashMap<>();
            response.put("key", "Event not found");
            return ResponseEntity.ok().body(response);
        }
        
        int id = eventRepository.findByeventName(ent.get("eventname")).getId();

        if(subEventRepository.existsByEventIdAndSubEventName(id,ent.get("subeventname"))){
            Map<String, String> response = new HashMap<>();
            response.put("key", "SubEvent Already exists ");
            return ResponseEntity.ok().body(response);
        }
        SubEvent subEvent = new SubEvent();
        subEvent.setEventId(eventRepository.findByeventName(ent.get("eventname")).getId());
        subEvent.setSubEventName(ent.get("subeventname"));
        subEvent.setStartTime(ent.get("starttime"));
        subEvent.setEndTime(ent.get("endtime"));
        subEvent.setTicketPrice(Integer.parseInt(ent.get("ticketprice")));
        subEvent.setTicketLimit(Integer.parseInt(ent.get("limit")));
        subEvent.setStartDate(ent.get("startdate1"));
        subEvent.setEndDate(ent.get("enddate1"));
        subEventRepository.save(subEvent);

        Map<String, String> response = new HashMap<>();
        response.put("key", "Sub Event Added successfully");
        return ResponseEntity.ok().body(response);
    }

    // Edit Event
    public ResponseEntity<?> editEvent(Map<String, String> ent) {

        Event event = eventRepository.findByeventName(ent.get("eventname"));
        event.setEventName(ent.get("neweventname"));
        event.setStartDate(ent.get("startdate"));
        event.setEndDate(ent.get("enddate"));
        event.setLocation(ent.get("location"));
        event.setOrganizerName(ent.get("organizername"));
        eventRepository.save(event);
        Map<String, String> response = new HashMap<>();
        response.put("key", "Event Edited successfully");
        return ResponseEntity.ok().body(response);
        
    }

    // Edit Sub Event
    public ResponseEntity<?> editSubEvent(Map<String,String> ent){
        SubEvent subEvent=subEventRepository.findByEventIdAndSubEventName(eventRepository.findByeventName(ent.get("Event Name")).getId(),ent.get("Subevent Name"));
        subEvent.setSubEventName(ent.get("Subevent Name"));
        subEvent.setStartTime(ent.get("Start Time"));
        subEvent.setEndTime(ent.get("End Time"));
        subEvent.setTicketPrice(Integer.parseInt(ent.get("Ticket Price")));
        subEvent.setTicketLimit(Integer.parseInt(ent.get("Limit")));
        subEvent.setStartDate(ent.get("Start Date"));
        subEvent.setEndDate(ent.get("End Date"));
        subeventHistoryRepository.updatesubeventName(ent.get("Old SubeventName"),ent.get("Subebvent name"));
        subEventRepository.save(subEvent);
        Map<String,String> response=new HashMap<>();
        response.put("message","Event Edited successfully");
        return ResponseEntity.ok().body(response);
    }

    // Delete Event
    public ResponseEntity<?> deleteEvent(Map<String, String> ent) {
        Event event = eventRepository.findByeventName(ent.get("eventname"));
        Iterable<SubEvent> subEvent = subEventRepository.findAllByeventId(event.getId());
        Iterator<SubEvent> itr = subEvent.iterator();
        while (itr.hasNext()) {
            SubEvent temp = itr.next();
            subEventRepository.delete(temp);
        }
        eventRepository.delete(event);

        Map<String, String> response = new HashMap<>();
        response.put("key", "Event Deleted successfully");
        return ResponseEntity.ok().body(response);
    }

    //Delete Sub Event
    public ResponseEntity<?> deleteSubEvent(Map<String,String> ent){
        SubEvent subEvent=subEventRepository.findByEventIdAndSubEventName(eventRepository.findByeventName(ent.get("eventname")).getId(),ent.get("subeventname"));
        subEventRepository.delete(subEvent);

        Map<String, String> response = new HashMap<>();
        response.put("key", "SubEvent Deleted successfully");
        return ResponseEntity.ok().body(response);
    }

    // Mark As Feature
    public ResponseEntity<?> markAsFeature(Map<String, String> ent) {
        Event event = eventRepository.findByeventName(ent.get("eventname"));
        if (event.getFeatured() == 0)
            event.setFeatured(1);
        event.setFeatured(0);

        Map<String, String> response = new HashMap<>();
        response.put("key ", "Event Featured successfully");
        return ResponseEntity.ok().body(response);
    }

    // Search By Event Name
    public ResponseEntity<?> searchByEventName(Map<String,String> ent){
        return eventService.getSearchEvent(ent);
    }

}
