package com.incubators.eventmanagement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.incubators.eventmanagement.data.model.User;

public interface UserRepository extends JpaRepository<User,String> {
    
}
