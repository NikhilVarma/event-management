package com.incubators.eventmanagement.data.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.incubators.eventmanagement.data.model.Event;

public interface EventRepository extends JpaRepository<Event,Integer>{
    public boolean existsByeventName(String event);
    public Event findByeventName(String name);

    @Query(value = "SELECT  e FROM Event e WHERE featured = 1")
    public Page<Event> findFeaturedEvent(Pageable pageable);
    
}
