package com.incubators.eventmanagement.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.incubators.eventmanagement.data.model.SubeventHistory;

import jakarta.transaction.Transactional;

public interface SubeventHistoryRepository extends JpaRepository<SubeventHistory, Integer> {

    @Query(value = "SELECT * FROM subevent_history sh WHERE sh.mobile_number = :MobileNumber  and sh.subevent_name = :SubeventName  and sh.main_event_name = :MainEventName ",nativeQuery = true)
    public List<SubeventHistory>existsByMobileNumberAndSubeventNameAndMainEventName( String MobileNumber , String SubeventName , String MainEventName);

    @Transactional
    @Modifying
    @Query(value = "UDATE * FROM subevent_history sh SET sh.subevent_name = :newSubEventName WHERE sh.subevent_name = :oldSubEventName",nativeQuery = true)
    public void updatesubeventName(String oldSubEventName,String newSubEventName);
}
