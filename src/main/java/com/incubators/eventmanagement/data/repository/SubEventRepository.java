package com.incubators.eventmanagement.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.incubators.eventmanagement.data.model.SubEvent;

public interface SubEventRepository extends JpaRepository<SubEvent,Integer>{
    public List<SubEvent> findAllByeventId(Integer id);
    //public SubEvent findByeventIdAndsubEventName(Integer id,String name);

    public boolean existsByEventIdAndSubEventName(Integer eventId , String subEventName);

    public SubEvent findByEventIdAndSubEventName(Integer eventId , String subEventName);

}
