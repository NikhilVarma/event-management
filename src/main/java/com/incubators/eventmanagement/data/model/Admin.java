package com.incubators.eventmanagement.data.model;


import org.springframework.stereotype.Component;


@Component
public class Admin {
    private String admin="9091929394";
    private String pass="1234";
    
    public String getAdmin() {
        return admin;
    }
    public void setAdmin(String admin) {
        this.admin = admin;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
}
