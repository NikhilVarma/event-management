package com.incubators.eventmanagement.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="user_info")
public class User {

    @Id
    @Column( name = "mobile_number" , unique = true , nullable = false )
    private String mobileNumber ;

    @Column( name = "first_name", nullable = true)
    private String firstName;

    @Column( name = "last_name", nullable = true)
    private String lastName;

    @Column( name = "password" , nullable = true)
    private String password;

    @Column( name = "secret_question" , nullable = true)
    private String secretQuestion;

    @Column( name = "answer" , nullable = true)
    private String answer; 

    @Column(name = "login_status")
    private Integer logInStatus;
    
    public Integer getLogInStatus() {
        return logInStatus;
    }
    public void setLogInStatus(Integer logInStatus) {
        this.logInStatus = logInStatus;
    }
    
    public User() {
    }
    public User(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    public String getMobileNumber() {
        return mobileNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getSecretQuestion() {
        return secretQuestion;
    }
    public void setSecretQuestion(String secretQuestion) {
        this.secretQuestion = secretQuestion;
    }
    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }


}
