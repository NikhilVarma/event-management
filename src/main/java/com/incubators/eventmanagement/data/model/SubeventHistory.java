package com.incubators.eventmanagement.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "subevent_history")
public class SubeventHistory {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id" , nullable = false , updatable = false , unique = true)
    private Integer Id;
    
    @Column(name="mobile_number")
    private String MobileNumber;

    public String getMainEventName() {
        return MainEventName;
    }

    public void setMainEventName(String mainEventName) {
        MainEventName = mainEventName;
    }

    @Column(name = "subevent_name")
    private String SubeventName;

    @Column(name = "rating")
    private String Rating;

    @Column(name = "feedback")
    private String feedback;

    @Column(name = "main_event_name")
    private String MainEventName;


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getSubeventName() {
        return SubeventName;
    }

    public void setSubeventName(String subeventName) {
        SubeventName = subeventName;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    
}
