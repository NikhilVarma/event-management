package com.incubators.eventmanagement.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incubators.eventmanagement.service.AdminService;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    AdminService adminService;

    @PostMapping("/log-in")
    private ResponseEntity<?> adminLogIn(@RequestBody Map<String,String> ent){
        return adminService.adminLogin(ent);
    }
    @PostMapping("/create-event")
    private ResponseEntity<?> createEvent(@RequestBody Map<String,String> ent){
        return adminService.createEvent(ent);
    }

    @PostMapping("/add-sub-event")
    private ResponseEntity<?> addSubEvent(@RequestBody Map<String,String> ent){
        return adminService.addSubEvent(ent);
    }

    @PostMapping("/edit-event")
    private ResponseEntity<?> editEvent(@RequestBody Map<String,String> ent){
        return adminService.editEvent(ent);
    }

    @DeleteMapping("/delete-event")
    private ResponseEntity<?> deleteEvent(@RequestBody Map<String,String> ent){
        return adminService.deleteEvent(ent);
    }

    @DeleteMapping("/delete-sub-event")
    private ResponseEntity<?> deleteSubEvent(@RequestBody Map<String,String> ent){
        return adminService.deleteSubEvent(ent);
    }
    
    @PostMapping("/mark-featured")
    private ResponseEntity<?> featured(@RequestBody Map<String,String> ent){
        return adminService.markAsFeature(ent);
    }

    @PostMapping("/search-event")
    private ResponseEntity<?> showAllEvents(@RequestBody Map<String,String> ent){
        return adminService.searchByEventName(ent);
    }
}
