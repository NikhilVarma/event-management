package com.incubators.eventmanagement.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incubators.eventmanagement.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    private UserService userService;

    @PostMapping("/sign-up")
    public ResponseEntity<?> signUp(@RequestBody Map<String,String> entity ){

        return  userService.signUp(entity);
    }

    @PostMapping("/log-in")
    public ResponseEntity<?> logIn(@RequestBody Map<String,String> entity){

        return userService.logIn(entity);
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<?> forgotPassword(@RequestBody Map<String,String> entity){

        return userService.forgotPassword(entity);
    }

    @PostMapping("/book-ticket")
    public ResponseEntity<?> bookTicket(@RequestBody Map<String,String> entity){

        return userService.bookTicket(entity);
    }

    @PostMapping("/add-rating")
    public ResponseEntity<?> addRating( @RequestBody Map<String , String> entity){

        return userService.addRating(entity);
    }

}
