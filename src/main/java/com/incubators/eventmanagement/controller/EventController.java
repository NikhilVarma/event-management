package com.incubators.eventmanagement.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incubators.eventmanagement.service.EventService;

@RestController
@RequestMapping("/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping("/main-event")
    public ResponseEntity<?> getEvents(@RequestBody Map<String , String > entity){

        if(entity.get("EventType").trim().equals("Latest")){
            return eventService.getLatestEvents(entity);
        }
        if (entity.get("EventType").trim().equals("Featured")) {
            return eventService.getFeaturedEvents(entity);
        }
        
        return eventService.getSearchEvent(entity);
    }

    @GetMapping("/sub-event")
    public ResponseEntity<?> getSubEvents(@RequestBody Map<String , String > entity){

        if(entity.get("EventType").trim().equals("Latest")){
            return eventService.getLatestSubEvents(entity);
        }
        return eventService.getSearchSubEvents(entity);
    }

}