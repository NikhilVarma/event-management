package com.incubators.eventmanagement.entryPoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.incubators.eventmanagement.data.repository")
@EntityScan("com.incubators.eventmanagement.data.model")
@ComponentScan(value = {"com.incubators.eventmanagement.controller","com.incubators.eventmanagement.service" , "com.incubators.eventmanagement.filter","com.incubators.eventmanagement.data.model"})
@SpringBootApplication
public class EventManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventManagementApplication.class, args);
	}

}
